-- +goose Up
CREATE TABLE accounts
(
    id varchar(60) PRIMARY KEY NOT NULL,
    balance double DEFAULT 0 NOT NULL
);

CREATE TABLE `account_logs` (
  `id` varchar(60) NOT NULL,
  `account_owner_id` varchar(60) NOT NULL,
  `debit_credit_flag` varchar(1) NOT NULL,
  `amount` double NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `account_logs_accounts_id_fk` (`account_owner_id`),
  CONSTRAINT `account_logs_accounts_id_fk` FOREIGN KEY (`account_owner_id`) REFERENCES `accounts` (`id`)
);



-- +goose Down
DROP TABLE account_logs;
DROP TABLE accounts;
