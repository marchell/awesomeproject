package logics

import (
	"awesomeProject/logics/models"
	"awesomeProject/repositories/interfaces"
)

type AccountLogLogic interface {
	// deposit
	CreditAccount(accountId string, amount float64) models.AccountLogModel
	GetAllAccountLogs(accountId string) []models.AccountLogModel
}

type AccountLogLogicImpl struct {
	accountLogRepository interfaces.AccountLogRepository
}

func NewAccountLogLogicImpl(accountLogRepository interfaces.AccountLogRepository) *AccountLogLogicImpl {
	return &AccountLogLogicImpl{
		accountLogRepository: accountLogRepository,
	}
}

func (a *AccountLogLogicImpl) CreditAccount(accountId string, amount float64) models.AccountLogModel {
	log, e := a.accountLogRepository.CreditAccount(accountId, amount)

	if e != nil {
		panic(e)
	}

	return models.AccountLogModelFrom(log)
}

func (a *AccountLogLogicImpl) GetAllAccountLogs(accountId string) []models.AccountLogModel {
	logs, e := a.accountLogRepository.GetAllAccountLogs(accountId)

	if e != nil {
		panic(e)
	}

	return models.AccountLogModelsFrom(logs)
}
