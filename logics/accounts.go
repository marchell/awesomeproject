package logics

import (
	"awesomeProject/logics/models"
	"awesomeProject/repositories/interfaces"
	repoModel "awesomeProject/repositories/models"
	"github.com/satori/go.uuid"
)

type AccountLogic interface {
	CreateAccount(account models.AccountModel) models.AccountModel
	GetAccount(accountId string) (account models.AccountModel)
}

type AccountLogicImpl struct {
	accountRepository interfaces.AccountRepository
}

func NewAccountLogicImpl(accountRepository interfaces.AccountRepository) *AccountLogicImpl {
	return &AccountLogicImpl{
		accountRepository: accountRepository,
	}
}

func (a *AccountLogicImpl) CreateAccount(account models.AccountModel) models.AccountModel {
	acc := repoModel.AccountModel{
		Balance: 0,
		Id:      uuid.NewV4().String(),
	}
	acc, err := a.accountRepository.CreateAccount(acc)

	if err != nil {
		panic(err)
	}

	return models.AccountModelFrom(acc)
}

func (a *AccountLogicImpl) GetAccount(accountId string) (account models.AccountModel) {
	acc, err := a.accountRepository.GetAccount(accountId)

	if err != nil {
		panic(err)
	}

	return models.AccountModelFrom(acc)
}
