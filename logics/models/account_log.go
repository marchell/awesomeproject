package models

import (
	repoModel "awesomeProject/repositories/models"
	"time"
)

type DebitCreditIndicator string

const (
	Debit  DebitCreditIndicator = "D"
	Credit DebitCreditIndicator = "C"
)

func debitCreditIndicatorFrom(indicator repoModel.DebitCreditIndicator) DebitCreditIndicator {
	switch indicator {
	case repoModel.Debit:
		return Debit
	case repoModel.Credit:
		return Credit
	}

	panic("not found")
}

type AccountLogModel struct {
	Id              string
	AccountOwnerId  string
	DebitCreditFlag DebitCreditIndicator
	Amount          float64
	CreatedAt       time.Time
}

func AccountLogModelFrom(repoModel repoModel.AccountLogModel) AccountLogModel {
	return AccountLogModel{
		Id:              repoModel.Id,
		DebitCreditFlag: debitCreditIndicatorFrom(repoModel.DebitCreditFlag),
		CreatedAt:       repoModel.CreatedAt,
		Amount:          repoModel.Amount,
		AccountOwnerId:  repoModel.AccountOwnerId,
	}
}

func AccountLogModelsFrom(repoLogs []repoModel.AccountLogModel) []AccountLogModel {
	logs := []AccountLogModel{}
	for _, log := range repoLogs {
		logs = append(logs, AccountLogModelFrom(log))
	}

	return logs
}
