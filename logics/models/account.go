package models

import "awesomeProject/repositories/models"

type AccountModel struct {
	Id      string
	Balance float64
}

func AccountModelFrom(repoModel models.AccountModel) AccountModel {
	return AccountModel{
		Id:      repoModel.Id,
		Balance: repoModel.Balance,
	}
}
