package routes

import (
	"awesomeProject/logics"
	"awesomeProject/routes/models"
	"github.com/gin-gonic/gin"
	"net/http"
)

type AccountLogRoute struct {
	accountLogLogic logics.AccountLogLogic
}

func NewAccountLogRoute(accountLogLogic logics.AccountLogLogic) AccountLogRoute {
	return AccountLogRoute{
		accountLogLogic: accountLogLogic,
	}
}

func (ar *AccountLogRoute) HandleCreditAccount(c *gin.Context) {
	accountId := c.Param("id")

	var request models.CreditAccountRequest
	err := c.ShouldBindJSON(&request)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"lala": err.Error()})
		return
	}

	log := ar.accountLogLogic.CreditAccount(accountId, request.Amount)

	c.JSON(http.StatusCreated, models.AccountLogResponseFrom(log))
}

func (ar *AccountLogRoute) HandleGetAllAccountLogs(c *gin.Context) {
	accountId := c.Param("id")

	logs := ar.accountLogLogic.GetAllAccountLogs(accountId)

	c.JSON(http.StatusOK, models.AccountLogResponsesFrom(logs))
}