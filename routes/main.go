package routes

import (
	"awesomeProject/logics"
	"github.com/gin-gonic/gin"
)

func NewRoutes(accountLogic logics.AccountLogic, accountLogLogic logics.AccountLogLogic) (*gin.Engine) {

	accountRoute := NewAccountRoute(accountLogic)
	accountLogRoute := NewAccountLogRoute(accountLogLogic)

	r := gin.Default()

	r.Use(func(context *gin.Context) {
		context.Header("Access-Control-Allow-Origin", "*")
	})

	r.POST("/accounts", accountRoute.HandleCreateAccount)
	r.GET("/accounts/:id", accountRoute.HandleGetAccount)

	r.POST("/accounts/:id/logs", accountLogRoute.HandleCreditAccount)
	r.GET("/accounts/:id/logs", accountLogRoute.HandleGetAllAccountLogs)

	return r
}
