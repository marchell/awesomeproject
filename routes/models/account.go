package models

import "awesomeProject/logics/models"

type AccountResponse struct {
	Id string `json:"id"`
	Balance float64 `json:"balance"`
}

func AccountResponseFrom(logicAccount models.AccountModel) AccountResponse {
	return AccountResponse{
		Id: logicAccount.Id,
		Balance: logicAccount.Balance,
	}
}