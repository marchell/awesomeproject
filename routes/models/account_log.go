package models

import (
	logicModel "awesomeProject/logics/models"
	"time"
)

type DebitCreditIndicator string

const (
	Debit  DebitCreditIndicator = "D"
	Credit DebitCreditIndicator = "C"
)

func debitCreditIndicatorFrom(indicator logicModel.DebitCreditIndicator) DebitCreditIndicator {
	switch indicator {
	case logicModel.Debit:
		return Debit
	case logicModel.Credit:
		return Credit
	}

	panic("not found")
}

type CreditAccountRequest struct {
	Amount float64 `json:"amount"`
}

type AccountLogResponse struct {
	Id              string               `json:"id"`
	AccountOwnerId  string               `json:"accountOwnerId"`
	DebitCreditFlag DebitCreditIndicator `json:"debitCreditFlag"`
	Amount          float64              `json:"amount"`
	CreatedAt       time.Time            `json:"createdAt"`
}

func AccountLogResponseFrom(model logicModel.AccountLogModel) AccountLogResponse {
	return AccountLogResponse{
		Id:              model.Id,
		AccountOwnerId:  model.AccountOwnerId,
		DebitCreditFlag: debitCreditIndicatorFrom(model.DebitCreditFlag),
		Amount:          model.Amount,
		CreatedAt:       model.CreatedAt,
	}
}

func AccountLogResponsesFrom(logicLogs []logicModel.AccountLogModel) []AccountLogResponse {
	logs := []AccountLogResponse{}
	for _, log := range logicLogs {
		logs = append(logs, AccountLogResponseFrom(log))
	}

	return logs
}
