package routes

import (
	"awesomeProject/logics"
	logicModels "awesomeProject/logics/models"
	models "awesomeProject/routes/models"
	"github.com/gin-gonic/gin"
	"net/http"
)

type AccountRoute struct {
	accountLogic logics.AccountLogic
}

func NewAccountRoute(accountLogic logics.AccountLogic) AccountRoute {
	return AccountRoute{
		accountLogic: accountLogic,
	}
}

func (ar *AccountRoute) HandleCreateAccount(c *gin.Context) {
	account := ar.accountLogic.CreateAccount(logicModels.AccountModel{})

	c.JSON(http.StatusCreated, models.AccountResponseFrom(account))
}

func (ar *AccountRoute) HandleGetAccount(c *gin.Context) {
	accountId := c.Param("id")
	account := ar.accountLogic.GetAccount(accountId)

	c.JSON(http.StatusOK, models.AccountResponseFrom(account))
}
