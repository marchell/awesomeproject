.PHONY: all

DIR=$(pwd)

setup:
	docker-compose up -d

image:
	docker build -t awesomeproject .

start:
	docker run --rm -p 8765:8765 awesomeproject

migrate:
	docker run --rm --network host -w /go/src/awesomeProject/migration awesomeproject /bin/bash -c "pwd; ls -la; goose --dir=migrations mysql \"root:rootpw@tcp(localhost:3380)/awesome\" up"
