FROM golang
RUN mkdir /go/src/awesomeProject
ADD . /go/src/awesomeProject
WORKDIR /go/src/awesomeProject
RUN go get -u github.com/kardianos/govendor && go get -u github.com/pressly/goose/cmd/goose && govendor sync && govendor build
CMD ["./awesomeProject"]