make setup
make image
make migrate
make start






db diagram:

+ accounts
    - id
    - balance


+ account_logs
    - id
    - account_owner_id (fk to accounts.id)
    - debit_credit_flag
    - amount
    - created_at