package main

import (
	"awesomeProject/logics"
	"awesomeProject/repositories/mysql_repository"
	"awesomeProject/routes"

)

func main()  {
	c := mysql_repository.OpenDbConnection()
	defer c.Close()

	// repos
	accountRepository := mysql_repository.NewAccountMysqlRepository(c)
	accountLogRepository := mysql_repository.NewAccountLogMysqlRepository(c)

	// logics

	accountsLogic := logics.NewAccountLogicImpl(accountRepository)
	accountsLogLogic := logics.NewAccountLogLogicImpl(accountLogRepository)

	// routes
	r := routes.NewRoutes(accountsLogic, accountsLogLogic)

	r.Static("/ui", "./public")
	r.Run(":8765")
}