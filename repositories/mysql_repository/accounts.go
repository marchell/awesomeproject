package mysql_repository

import "awesomeProject/repositories/models"

type AccountMysqlRepository struct {
	connection Connection
}

func NewAccountMysqlRepository(connection Connection) *AccountMysqlRepository {
	return &AccountMysqlRepository{
		connection: connection,
	}
}

func (amr *AccountMysqlRepository) CreateAccount(account models.AccountModel) (models.AccountModel, error) {
	stmt, err := amr.connection.DB.Prepare("INSERT INTO accounts VALUES( ?, ? )") // ? = placeholder
	if err != nil {
		panic(err.Error())
	}
	defer stmt.Close()

	result, err := stmt.Exec(account.Id, account.Balance)

	if err != nil{
		panic(err)
	}

	if count, err := result.RowsAffected(); err != nil || count != 1 {
		panic("something wrong inserting new acc")
	}

	return account, nil
}

func (amr *AccountMysqlRepository) GetAccount(accountId string) (models.AccountModel, error) {
	stmt, err := amr.connection.DB.Prepare("SELECT id, balance FROM accounts where id= ?")
	if err != nil {
		panic(err.Error())
	}
	defer stmt.Close()

	row := stmt.QueryRow(accountId)

	res := models.AccountModel{}
	err = row.Scan(&res.Id, &res.Balance)

	if err != nil {
		panic(err)
	}

	return res, nil
}
