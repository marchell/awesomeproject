package mysql_repository

import (
	_ "github.com/go-sql-driver/mysql" // for sqlx
	"github.com/jmoiron/sqlx"
)

type Connection struct {
	DB *sqlx.DB
}

func (c *Connection) Close()  {
	c.DB.Close()
}

func OpenDbConnection() Connection {

	dbUsername := "root"
	dbPassword := "rootpw"
	dbHost := "0.0.0.0"
	dbPort := "3380"
	dbName := "awesome"
	dataSourceName := dbUsername + ":" + dbPassword + "@(" + dbHost + ":" + (string(dbPort)) + ")/" + dbName + "?parseTime=true"
	db, err := sqlx.Open("mysql", dataSourceName)

	if err != nil {
		panic(err)
	}

	err = db.Ping()

	if err != nil {
		panic(err.Error())
	}

	return Connection{
		DB: db,
	}
}
