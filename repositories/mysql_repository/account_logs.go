package mysql_repository

import (
	"awesomeProject/repositories/models"
	"fmt"
	"github.com/satori/go.uuid"
	"time"
)

type AccountLogMysqlRepository struct{
	connection Connection
}

func NewAccountLogMysqlRepository(connection Connection) *AccountLogMysqlRepository {
	return &AccountLogMysqlRepository{
		connection: connection,
	}
}

func (almr *AccountLogMysqlRepository) CreditAccount(accountId string, amount float64) (models.AccountLogModel, error) {

	tx := almr.connection.DB.MustBegin()

	lala := models.AccountLogModel{
		Id: uuid.NewV4().String(),
		Amount: amount,
		AccountOwnerId:accountId,
		CreatedAt: time.Now(),
		DebitCreditFlag: "D",
	}
	_, err := tx.NamedExec("INSERT INTO account_logs (id, account_owner_id, debit_credit_flag, amount, created_at) VALUES (:id, :account_owner_id, :debit_credit_flag, :amount, :created_at)", &lala)

	if err != nil {
		tx.Rollback()
		panic(err)
	}

	result := tx.MustExec("UPDATE accounts SET balance= balance + '"+fmt.Sprintf("%f", lala.Amount)+"' WHERE id = '" + lala.AccountOwnerId+"'")

	if count, err := result.RowsAffected(); err != nil || count == 0 {
		tx.Rollback()
		panic(err)
	}

	tx.Commit()
	return lala, nil
}

func (almr *AccountLogMysqlRepository) GetAllAccountLogs(accountId string) ([]models.AccountLogModel, error) {
	lala := []models.AccountLogModel{}

	err := almr.connection.DB.Select(&lala, "SELECT * FROM account_logs ORDER BY created_at DESC")
	if err != nil {
		panic(err)
	}

	return lala, nil
}
