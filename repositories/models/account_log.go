package models

import "time"

type DebitCreditIndicator string

const (
	Debit  DebitCreditIndicator = "D"
	Credit DebitCreditIndicator = "C"
)

type AccountLogModel struct {
	Id              string               `db:"id"`
	AccountOwnerId  string               `db:"account_owner_id"`
	DebitCreditFlag DebitCreditIndicator `db:"debit_credit_flag"`
	Amount          float64              `db:"amount"`
	CreatedAt       time.Time            `db:"created_at"`
}
