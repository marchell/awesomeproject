package interfaces

import "awesomeProject/repositories/models"

type AccountLogRepository interface {
	// deposit
	CreditAccount(accountId string, amount float64) (models.AccountLogModel, error)
	GetAllAccountLogs(accountId string) ([]models.AccountLogModel, error)
}
