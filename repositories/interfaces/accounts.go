package interfaces

import "awesomeProject/repositories/models"

type AccountRepository interface {
	CreateAccount(account models.AccountModel) (models.AccountModel, error)
	GetAccount(accountId string) (models.AccountModel, error)
}
